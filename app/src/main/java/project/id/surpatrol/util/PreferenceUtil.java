package project.id.surpatrol.util;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

public class PreferenceUtil {
    private static final String TAG = PreferenceUtil.class.getSimpleName();

    private static final String USERID_PREF = "pref_userid";
    private static final String USERNAME_PREF = "pref_username";
    private static final String IP_PREF = "pref_ip";

    public static @Nullable void setUserId(Context context, int id) {
        setIntegerPrefrence(context, USERID_PREF, id);
    }

    public static @Nullable int getUserId(Context context) {
        return getIntegerPreference(context, USERID_PREF, 0);
    }

    public static @Nullable void setUserName(Context context, String name) {
        setStringPreference(context, USERNAME_PREF, name);
    }

    public static @Nullable String getUserName(Context context) {
        return getStringPreference(context, USERNAME_PREF, null);
    }

    public static @Nullable void setIpAddress(Context context, String ip) {
        setStringPreference(context, IP_PREF, ip);
    }

    public static @Nullable String getIpAddress(Context context) {
        return getStringPreference(context, IP_PREF, "");
    }

    public static void setStringPreference(Context context, String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    public static String getStringPreference(Context context, String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaultValue);
    }

    private static int getIntegerPreference(Context context, String key, int defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaultValue);
    }

    private static void setIntegerPrefrence(Context context, String key, int value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key, value).apply();
    }

}
