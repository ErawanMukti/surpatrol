package project.id.surpatrol;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.common.base.Optional;

import project.id.surpatrol.activity.login.LoginActivity;
import project.id.surpatrol.activity.main.MainActivity;
import project.id.surpatrol.util.PreferenceUtil;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        String ip_pref = PreferenceUtil.getIpAddress(getBaseContext());
        if ( ip_pref.equals("") ) {
            PreferenceUtil.setIpAddress(getBaseContext(), getResources().getString(R.string.ip_address));
        }

        Handler handler = new Handler();
        handler.postDelayed(splash, 1000);
    }

    Runnable splash = new Runnable() {
        @Override
        public void run() {
            int userId = PreferenceUtil.getUserId(getBaseContext());

            if ( userId > 0 ) {
                startActivity(new Intent(getBaseContext(), MainActivity.class));
            } else {
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
            }
            finish();
        }
    };
}
