package project.id.surpatrol;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import project.id.surpatrol.databases.CheckpointTable;
import project.id.surpatrol.databases.PatroliTable;
import project.id.surpatrol.model.Patroli;
import project.id.surpatrol.model.UploadResponse;
import project.id.surpatrol.rest.ApiClient;
import project.id.surpatrol.rest.ApiInterface;
import project.id.surpatrol.util.PreferenceUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadActivity extends AppCompatActivity {
    private LinearLayout layRoot;
    private ListView listView;
    private Dialog loadingDialog;
    private ApiInterface mApiInterface;
    private SimpleAdapter adapter;
    private List<HashMap<String, String>> fillMaps;
    private boolean isSuccess = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        initializeResources();
        initializeToolbar();
        setupListView();

        String ip = PreferenceUtil.getIpAddress(getBaseContext());
        mApiInterface = ApiClient.getClient(ip).create(ApiInterface.class);
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_upload, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_upload:
                doUpload();
                break;
        }
        return super.onOptionsItemSelected(menu);
    }

    private void doUpload() {
        isSuccess = true;
        loadingDialog = ProgressDialog.show(UploadActivity.this, "Harap Tunggu", "Proses Upload Hasil Patroli...");
        loadingDialog.setCanceledOnTouchOutside(true);

        List<Patroli> patroliList = new PatroliTable().selectAll();
        for (final Patroli patroli : patroliList) {
            List<MultipartBody.Part> parts = new ArrayList<>();

            File fileFoto = new File(patroli.getFoto());
            RequestBody rbFoto = RequestBody.create(MediaType.parse("image/*"), fileFoto);
            parts.add(MultipartBody.Part.createFormData("gambar[foto]", fileFoto.getName(), rbFoto));

            Gson gson = new Gson();
            String data = gson.toJson(patroli);
            RequestBody description = RequestBody.create(MultipartBody.FORM, data);

            Call<ResponseBody> userCall = mApiInterface.uploadPatroli(description, parts);
            userCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    loadingDialog.dismiss();
                    if (response.isSuccessful()) {
                        String responseBody = response.message();

                        if (responseBody.equals("OK")) {
                            new PatroliTable().deleteSingle(patroli.getPatroliId());
                            Snackbar.make(layRoot, "Upload hasil patroli selesai", Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(layRoot, "Upload hasil patroli gagal", Snackbar.LENGTH_LONG).show();
                            isSuccess = false;
                        }
                    } else {
                        Snackbar.make(layRoot, "Terjadi kesalahan koneksi, silahkan ulangi beberapa saat lagi", Snackbar.LENGTH_LONG).show();
                        Log.d("UPLOAD_ERROR", response.message());
                        isSuccess = false;
                        return;
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    loadingDialog.dismiss();
                    Snackbar.make(layRoot, "Terjadi kesalahan koneksi, silahkan ulangi beberapa saat lagi", Snackbar.LENGTH_LONG).show();
                    Log.d("API_ERROR", t.getMessage().toString());
                    isSuccess = false;
                    return;
                }
            });
        }
    }

    private void initializeToolbar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loadData() {
        PatroliTable patroliTable = new PatroliTable();
        List<Patroli> patroliList = patroliTable.selectAll();

        fillMaps.clear();
        for (Patroli patroli : patroliList) {
            HashMap<String,String> hm = new HashMap<String,String>();
            hm.put("Tanggal", patroli.getTanggal());
            hm.put("Shift", patroli.getShift());
            hm.put("Checkpoint", new CheckpointTable().getCheckpointName(patroli.getCheckpointId()));
            fillMaps.add(hm);
        }
        adapter.notifyDataSetChanged();
    }

    private void setupListView() {
        String[] from = new String[] {"Tanggal", "Shift", "Checkpoint"};
        int[] to = new int[] { R.id.txtGridTanggal, R.id.txtGridShift, R.id.txtGridCheckpoint};

        fillMaps = new ArrayList<HashMap<String, String>>();
        adapter = new SimpleAdapter(this, fillMaps, R.layout.grid_hasil_patroli, from, to);
        listView.setAdapter(adapter);
    }

    private void initializeResources() {
        layRoot  = findViewById(R.id.layUploadRoot);
        listView = findViewById(R.id.listHasilPatroli);
    }
}
