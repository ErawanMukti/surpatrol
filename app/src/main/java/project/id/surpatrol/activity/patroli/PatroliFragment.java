package project.id.surpatrol.activity.patroli;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;
import project.id.surpatrol.R;
import project.id.surpatrol.activity.formpatroli.FormPatroliActivity;

import static com.google.common.base.Preconditions.checkNotNull;

public class PatroliFragment extends Fragment implements PatroliContract.View {

    private SurfaceView mySurfaceView;
    private QREader qrEader;
    private TextView lblCheckpoint;
    private LinearLayout layButton;
    private Button btnAddEvent, btnFinish;

    private PatroliContract.Presenter mPresenter;

    public PatroliFragment() {

    }

    public static PatroliFragment newInstance() {
        return new PatroliFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
        qrEader.initAndStart(mySurfaceView);
    }

    @Override
    public void onPause() {
        super.onPause();
        qrEader.releaseAndCleanup();
    }

    @Override
    public void setPresenter(PatroliContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patroli, container, false);

        initializeResources(view);

        return view;
    }

    private void initializeResources(View view) {
        mySurfaceView = view.findViewById(R.id.camera_view);
        lblCheckpoint = view.findViewById(R.id.lblCheckpoint);
        layButton     = view.findViewById(R.id.layButton);
        btnAddEvent   = view.findViewById(R.id.btnAddEvent);
        btnFinish     = view.findViewById(R.id.btnFinish);

        layButton.setVisibility(View.GONE);
        btnAddEvent.setOnClickListener(new AddEventListener());
        btnFinish.setOnClickListener(new FinishListener());

        qrEader = new QREader.Builder(getContext(), mySurfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                Log.d("QREader", "Value : " + data);
                lblCheckpoint.post(new Runnable() {
                    @Override
                    public void run() {
                        mPresenter.getCheckpoint(data);
                    }
                });
            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(mySurfaceView.getHeight())
                .width(mySurfaceView.getWidth())
                .build();
    }

    @Override
    public void showCheckpointLabel(String message) {
        lblCheckpoint.setText(message);
        layButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPatroliForm(int checkpointId) {
        Intent intent = new Intent(getContext(), FormPatroliActivity.class);
        intent.putExtra("checkpoint", checkpointId);
        startActivity(intent);
        getActivity().finish();
    }

    private class AddEventListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            mPresenter.openPatroliForm();
        }
    }

    private class FinishListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            getActivity().finish();
        }
    }
}
