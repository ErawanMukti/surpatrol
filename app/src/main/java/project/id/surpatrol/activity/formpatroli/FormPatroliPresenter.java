package project.id.surpatrol.activity.formpatroli;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import project.id.surpatrol.databases.EventTable;
import project.id.surpatrol.databases.PatroliTable;
import project.id.surpatrol.model.Event;
import project.id.surpatrol.model.Patroli;

import static com.google.common.base.Preconditions.checkNotNull;

public class FormPatroliPresenter implements FormPatroliContract.Presenter {

    private final FormPatroliContract.View mView;
    private String[] arr_shift = {"Pagi", "Siang", "Malam"};
    private List<Event> eventList;
    private int mCheckpointId;

    public FormPatroliPresenter(@Nullable int checkpointId, @NonNull FormPatroliContract.View view) {
        mCheckpointId = checkpointId;
        mView = checkNotNull(view, "Form Patroli View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void loadDataEvent() {
        EventTable eventTable = new EventTable();
        eventList = eventTable.selectAll();
        mView.setEventSpinner(eventList);
    }

    @Override
    public void loadDataShift() {
        mView.setShiftSpinner(arr_shift);
    }

    @Override
    public void savePatroli(int user, int selected_event, String tanggal, String patroliTime,
                            String shift, String ket, String foto) {
        int event = eventList.get(selected_event).getEventId();

        Patroli patroli = new Patroli(user, event, mCheckpointId, tanggal, patroliTime, shift, ket, foto);
        PatroliTable patroliTable = new PatroliTable();
        int patroliId = patroliTable.insert(patroli);

        if ( patroliId > 0) {
            mView.showSaveMessage("Hasil patroli berhasil disimpan");
            mView.finishPatroli();
        } else {
            mView.showSaveMessage("Terjadi kesalahan pada saat menyimpan hasil patroli. Silahkan ulangi");
        }
    }
}
