package project.id.surpatrol.activity.login;

import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import project.id.surpatrol.databases.UserTable;
import project.id.surpatrol.model.User;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mView;

    public LoginPresenter(@NonNull LoginContract.View view) {
        mView = checkNotNull(view, "Login View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void login(String username, String password) {
        if ( username.isEmpty() || password.isEmpty() ) {
            mView.showErrorLogin("Silahkan lengkapi username / password");
            return;
        }

        UserTable userTable = new UserTable();
        User user = userTable.selectSingle(username, password);
        Optional<Integer> userId = Optional.fromNullable(user.getUserId());

        if ( userId.isPresent() && userId.get() > 0 ) {
            mView.showMain(userId.get(), user.getUserName());
        } else {
            mView.showErrorLogin("Username / password salah");
        }

    }
}
