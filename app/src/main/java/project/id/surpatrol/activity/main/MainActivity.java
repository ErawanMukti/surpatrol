package project.id.surpatrol.activity.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import project.id.surpatrol.DataAreaActivity;
import project.id.surpatrol.DataCheckpointActivity;
import project.id.surpatrol.DataEventActivity;
import project.id.surpatrol.activity.login.LoginActivity;
import project.id.surpatrol.R;
import project.id.surpatrol.UploadActivity;
import project.id.surpatrol.util.ActivityUtils;
import project.id.surpatrol.util.PreferenceUtil;

public class MainActivity extends AppCompatActivity {

    private MainFragment mainFragment;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupFragment();
        setupPresenter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_utama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case R.id.action_area:
                startActivity(new Intent(getBaseContext(), DataAreaActivity.class));;
                break;

            case R.id.action_checkpoint:
                startActivity(new Intent(getBaseContext(), DataCheckpointActivity.class));;
                break;

            case R.id.action_event:
                startActivity(new Intent(getBaseContext(), DataEventActivity.class));;
                break;

            case R.id.action_upload:
                startActivity(new Intent(getBaseContext(), UploadActivity.class));;
                break;

            case R.id.action_logout:
                PreferenceUtil.setUserId(getBaseContext(), 0);
                PreferenceUtil.setUserName(getBaseContext(), "");

                startActivity(new Intent(getBaseContext(), LoginActivity.class));;
                finish();
                break;
        }
        return super.onOptionsItemSelected(menu);
    }

    private void setupPresenter() {
        mainPresenter = new MainPresenter(mainFragment);
    }

    private void setupFragment() {
        mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.frmMainContent);
        if (mainFragment == null) {
            // Create the fragment
            mainFragment = MainFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mainFragment, R.id.frmMainContent);
        }
    }
}
