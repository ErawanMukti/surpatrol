package project.id.surpatrol.activity.login;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import project.id.surpatrol.R;
import project.id.surpatrol.activity.main.MainActivity;
import project.id.surpatrol.util.PreferenceUtil;

import static com.google.common.base.Preconditions.checkNotNull;

public class LoginFragment extends Fragment implements LoginContract.View {

    private EditText txtUsername, txtPassword;
    private Button btnLogin, btnServer;

    private LoginContract.Presenter mPresenter;

    public LoginFragment() { }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        initializeResources(view);

        return view;
    }

    private void initializeResources(View view) {
        txtUsername = view.findViewById(R.id.txtLoginUsername);
        txtPassword = view.findViewById(R.id.txtLoginPassword);
        btnLogin    = view.findViewById(R.id.btnLogin);
        btnServer   = view.findViewById(R.id.btnLoginServer);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.login(txtUsername.getText().toString(), txtPassword.getText().toString());
            }
        });

        btnServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setTitle("Setting IP");

                final EditText input = new EditText(getContext());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                input.setPadding(8, 0, 8, 0);
                input.setText(PreferenceUtil.getIpAddress(getContext()));
                alertDialog.setView(input);

                alertDialog.setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String keterangan = input.getText().toString();
                        PreferenceUtil.setIpAddress(getContext(), keterangan);
                    }
                });

                alertDialog.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
            }
        });
    }

    public void showMain(int userId, String userName) {
        PreferenceUtil.setUserId(getContext(), userId);
        PreferenceUtil.setUserName(getContext(), userName);

        startActivity(new Intent(getContext(), MainActivity.class));
        getActivity().finish();
    }

    @Override
    public void showErrorLogin(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
