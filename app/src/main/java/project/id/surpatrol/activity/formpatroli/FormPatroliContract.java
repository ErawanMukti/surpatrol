package project.id.surpatrol.activity.formpatroli;

import java.util.List;

import project.id.surpatrol.BasePresenter;
import project.id.surpatrol.BaseView;
import project.id.surpatrol.model.Event;

public class FormPatroliContract {
    public interface View extends BaseView<Presenter> {
        public void setEventSpinner(List<Event> events);

        public void setShiftSpinner(String[] arr_shift);

        public void setTanggal();

        public void doSave();

        public void showSaveMessage(String message);

        public void finishPatroli();
    }

    interface Presenter extends BasePresenter {
        public void loadDataEvent();

        public void loadDataShift();

        public void savePatroli(int user, int selected_event, String tanggal,
                                String patroliTime, String shift, String ket, String foto);
    }
}
