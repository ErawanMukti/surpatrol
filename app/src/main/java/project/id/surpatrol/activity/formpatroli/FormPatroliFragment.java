package project.id.surpatrol.activity.formpatroli;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import project.id.surpatrol.R;
import project.id.surpatrol.model.Event;
import project.id.surpatrol.util.PreferenceUtil;

import static android.app.Activity.RESULT_OK;
import static com.google.common.base.Preconditions.checkNotNull;

public class FormPatroliFragment extends Fragment implements FormPatroliContract.View {

    private ImageView imgRuangan;
    private Button btnAmbilGambar;
    private EditText txtTanggal;
    private Spinner spnShift, spnEvent;
    private EditText txtKeterangan;

    public CameraImagePicker imgPicker;
    private Calendar myCalendar = Calendar.getInstance();
    private String patroliTime;
    private String foto_path = "";

    private FormPatroliContract.Presenter mPresenter;

    public FormPatroliFragment() { }

    public static FormPatroliFragment newInstance() {
        return new FormPatroliFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void setPresenter(FormPatroliContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_patroli, container, false);

        initializeResources(view);

        mPresenter.loadDataShift();
        mPresenter.loadDataEvent();

        return view;
    }

    private void initializeResources(View view) {
        imgRuangan = view.findViewById(R.id.imgFormPatroli);
        btnAmbilGambar = view.findViewById(R.id.btnAmbilGambar);
        txtTanggal = view.findViewById(R.id.txtTanggal);
        spnShift = view.findViewById(R.id.spnShift);
        spnEvent = view.findViewById(R.id.spnEvent);
        txtKeterangan = view.findViewById(R.id.txtKeterangan);

        txtTanggal.setInputType(InputType.TYPE_NULL);
        txtTanggal.setOnClickListener(new TanggalListener());
        btnAmbilGambar.setOnClickListener(new ambilGambarListener());
    }

    @Override
    public void setEventSpinner(List<Event> events) {
        ArrayAdapter<Event> adapterEvent = new ArrayAdapter<Event>(getContext(), android.R.layout.simple_spinner_dropdown_item, events);
        spnEvent.setAdapter(adapterEvent);
    }

    @Override
    public void setShiftSpinner(String[] arr_shift) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, arr_shift);
        spnShift.setAdapter(adapter);
    }

    @Override
    public void setTanggal() {
        String myDateFormat = "yyyy-MM-dd";
        String myTimeFormat = "HH:mm:ss";

        SimpleDateFormat dateSdf = new SimpleDateFormat(myDateFormat);
        SimpleDateFormat timeSdf = new SimpleDateFormat(myTimeFormat);

        txtTanggal.setText(dateSdf.format(myCalendar.getTime()));
        patroliTime = timeSdf.format(myCalendar.getTime());
    }

    @Override
    public void doSave() {
        int user = PreferenceUtil.getUserId(getContext());
        int selected_event = spnEvent.getSelectedItemPosition();
        String tanggal = txtTanggal.getText().toString();
        String shift = spnShift.getSelectedItem().toString();
        String ket = txtKeterangan.getText().toString();

        mPresenter.savePatroli(user, selected_event, tanggal, patroliTime, shift, ket, foto_path);
    }

    @Override
    public void showSaveMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishPatroli() {
        getActivity().finish();
    }

    private class TanggalListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            new DatePickerDialog(getContext(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();

        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            setTanggal();
        }
    };


    private class ambilGambarListener implements View.OnClickListener, ImagePickerCallback {
        @Override
        public void onClick(View v) {
            imgPicker = new CameraImagePicker(getActivity());
            imgPicker.setCacheLocation(CacheLocation.EXTERNAL_STORAGE_APP_DIR);
            imgPicker.setImagePickerCallback(this);
            imgPicker.shouldGenerateMetadata(true);
            imgPicker.pickImage();
        }

        @Override
        public void onImagesChosen(List<ChosenImage> images) {
            if ( images != null ) {
                for ( ChosenImage image : images ) {
                    String[] tmp = image.getDisplayName().split("\\.");
                    foto_path    = image.getOriginalPath();
                }

                File file = new File(foto_path);
                Glide.with(getActivity()).load(Uri.fromFile(file)).into(imgRuangan);
            }
        }

        @Override
        public void onError(String s) {
            Log.d("AMBIL_GAMBAR_ERROR", s);
        }
    }
}
