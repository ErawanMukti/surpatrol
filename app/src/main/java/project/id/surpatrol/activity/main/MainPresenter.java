package project.id.surpatrol.activity.main;

import android.support.annotation.NonNull;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainPresenter implements MainContract.Presenter {
    private final MainContract.View mView;

    public MainPresenter(@NonNull MainContract.View view) {
        mView = checkNotNull(view, "Main View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void newPatroli() {
        mView.showPatroli();
    }

    @Override
    public void start() {

    }
}
