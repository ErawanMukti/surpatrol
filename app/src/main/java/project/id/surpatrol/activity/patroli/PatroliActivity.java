package project.id.surpatrol.activity.patroli;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Optional;

import project.id.surpatrol.activity.formpatroli.FormPatroliActivity;
import project.id.surpatrol.R;
import project.id.surpatrol.databases.CheckpointTable;
import project.id.surpatrol.model.Checkpoint;
import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;
import project.id.surpatrol.util.ActivityUtils;

public class PatroliActivity extends AppCompatActivity {

    private PatroliFragment mFragment;
    private PatroliPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patroli);

        setupFragment();
        setupPresenter();
    }

    private void setupPresenter() {
        mPresenter = new PatroliPresenter(mFragment);
    }

    private void setupFragment() {
        mFragment = (PatroliFragment) getSupportFragmentManager().findFragmentById(R.id.frmPatroliContent);
        if (mFragment == null) {
            // Create the fragment
            mFragment = PatroliFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mFragment, R.id.frmPatroliContent);
        }
    }
}
