package project.id.surpatrol.activity.patroli;

import project.id.surpatrol.BasePresenter;
import project.id.surpatrol.BaseView;

public class PatroliContract {
    public interface View extends BaseView<Presenter> {
        public void showCheckpointLabel(String message);

        public void showPatroliForm(int checkpointId);
    }

    interface Presenter extends BasePresenter {
        public void getCheckpoint(String data);

        public void openPatroliForm();
    }
}
