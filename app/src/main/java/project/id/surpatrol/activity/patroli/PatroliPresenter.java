package project.id.surpatrol.activity.patroli;

import android.support.annotation.NonNull;
import android.view.View;

import com.google.common.base.Optional;

import project.id.surpatrol.databases.CheckpointTable;
import project.id.surpatrol.model.Checkpoint;

import static com.google.common.base.Preconditions.checkNotNull;

public class PatroliPresenter implements PatroliContract.Presenter {

    private final PatroliContract.View mView;
    private int checkpointId;

    public PatroliPresenter(@NonNull PatroliContract.View view) {
        mView = checkNotNull(view, "Patroli View Cannot Be Null!");
        mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getCheckpoint(String data) {
        CheckpointTable checkpointTable = new CheckpointTable();
        Checkpoint checkpoint = checkpointTable.selectSingleByCode(data);
        Optional<Integer> chId = Optional.fromNullable(checkpoint.getCpId());

        if ( chId.isPresent() && chId.get() > 0 ) {
            mView.showCheckpointLabel(data + "-" + checkpoint.getCpName());
            checkpointId = checkpoint.getCpId();
        } else {
            mView.showCheckpointLabel("Data checkpoint tidak ditemukan. Silahkan scan ulang");
        }

    }

    @Override
    public void openPatroliForm() {
        mView.showPatroliForm(checkpointId);
    }
}
