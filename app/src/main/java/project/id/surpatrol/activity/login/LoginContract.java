package project.id.surpatrol.activity.login;

import project.id.surpatrol.BasePresenter;
import project.id.surpatrol.BaseView;

public class LoginContract {
    public interface View extends BaseView<Presenter> {
        void showMain(int userId, String userName);

        void showErrorLogin(String message);
    }

    interface Presenter extends BasePresenter {
        void login(String username, String password);
    }
}
