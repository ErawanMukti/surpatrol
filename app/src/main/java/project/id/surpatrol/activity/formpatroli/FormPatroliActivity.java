package project.id.surpatrol.activity.formpatroli;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import project.id.surpatrol.R;
import project.id.surpatrol.databases.EventTable;
import project.id.surpatrol.databases.PatroliTable;
import project.id.surpatrol.model.Event;
import project.id.surpatrol.model.Patroli;
import project.id.surpatrol.util.ActivityUtils;
import project.id.surpatrol.util.PreferenceUtil;

public class FormPatroliActivity extends AppCompatActivity {

    private FormPatroliFragment mFragment;
    private FormPatroliPresenter mPresenter;
    private int checkpoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_patroli);

        initializeToolbar();

        Bundle extras = getIntent().getExtras();
        checkpoint = extras.getInt("checkpoint");

        setupFragment();
        setupPresenter();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                mFragment.imgPicker.submit(data);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_formpatroli, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menu) {
        switch (menu.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_simpan:
                mFragment.doSave();
                break;
        }
        return super.onOptionsItemSelected(menu);
    }

    private void initializeToolbar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setupPresenter() {
        mPresenter = new FormPatroliPresenter(checkpoint, mFragment);
    }

    private void setupFragment() {
        mFragment = (FormPatroliFragment) getSupportFragmentManager().findFragmentById(R.id.frmFormPatroliContent);
        if (mFragment == null) {
            // Create the fragment
            mFragment = FormPatroliFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mFragment, R.id.frmFormPatroliContent);
        }
    }
}
