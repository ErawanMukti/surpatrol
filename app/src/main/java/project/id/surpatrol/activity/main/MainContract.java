package project.id.surpatrol.activity.main;

import project.id.surpatrol.BasePresenter;
import project.id.surpatrol.BaseView;

public class MainContract {
    public interface View extends BaseView<Presenter> {
        void showPatroli();
    }

    interface Presenter extends BasePresenter {
        void newPatroli();
    }
}
