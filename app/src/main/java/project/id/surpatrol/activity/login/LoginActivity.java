package project.id.surpatrol.activity.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Optional;

import project.id.surpatrol.R;
import project.id.surpatrol.activity.main.MainActivity;
import project.id.surpatrol.databases.UserTable;
import project.id.surpatrol.model.User;
import project.id.surpatrol.util.ActivityUtils;
import project.id.surpatrol.util.PreferenceUtil;

public class LoginActivity extends AppCompatActivity {

    private LoginFragment mFragment;
    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupFragment();
        setupPresenter();
    }

    private void setupPresenter() {
        mPresenter = new LoginPresenter(mFragment);
    }

    private void setupFragment() {
        mFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.frmLoginContent);
        if (mFragment == null) {
            // Create the fragment
            mFragment = LoginFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mFragment, R.id.frmLoginContent);
        }
    }
}
