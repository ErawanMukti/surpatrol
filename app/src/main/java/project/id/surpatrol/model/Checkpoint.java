package project.id.surpatrol.model;

public class Checkpoint {
    public static final String TABLE = "checkpoints";

    public static final String COLUMN_ID = "cpid";
    public static final String COLUMN_GROUP = "cpgroup";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";

    private int cpId;
    private String cpGroup;
    private int cpCode;
    private String cpName;

    public Checkpoint() {}

    public Checkpoint(int id, String group, int code, String name) {
        this.cpId = id;
        this.cpGroup = group;
        this.cpCode = code;
        this.cpName = name;
    }

    public int getCpId() {
        return cpId;
    }

    public void setCpId(int cpId) {
        this.cpId = cpId;
    }

    public String getCpGroup() {
        return cpGroup;
    }

    public void setCpGroup(String cpGroup) {
        this.cpGroup = cpGroup;
    }

    public int getCpCode() {
        return cpCode;
    }

    public void setCpCode(int cpCode) {
        this.cpCode = cpCode;
    }

    public String getCpName() {
        return cpName;
    }

    public void setCpName(String cpName) {
        this.cpName = cpName;
    }

    @Override
    public String toString() {
        return cpName;
    }
}
