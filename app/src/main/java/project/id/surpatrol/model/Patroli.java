package project.id.surpatrol.model;

public class Patroli {
    public static final String TABLE = "patroli";

    public static final String COLUMN_ID = "patroliid";
    public static final String COLUMN_PATROLMAN = "patrolmanid";
    public static final String COLUMN_EVENT = "eventid";
    public static final String COLUMN_CHECKPOINT = "checkpointid";
    public static final String COLUMN_TANGGAL = "tanggal";
    public static final String COLUMN_JAM = "jam";
    public static final String COLUMN_SHIFT = "shift";
    public static final String COLUMN_KET = "keterangan";
    public static final String COLUMN_FOTO = "foto";

    private int patroliId;
    private int patrolmanId;
    private int eventId;
    private int checkpointId;
    private String tanggal;
    private String jam;
    private String shift;
    private String keterangan;
    private String foto;

    public Patroli() { }

    public Patroli(int id, int patrolman, int event, int checkpoint, String tanggal, String jam, String shift, String ket) {
        this.patroliId = id;
        this.patrolmanId = patrolman;
        this.eventId = event;
        this.checkpointId = checkpoint;
        this.tanggal = tanggal;
        this.jam = tanggal;
        this.shift = shift;
        this.keterangan = ket;
    }

    public Patroli(int patrolman, int event, int checkpoint, String tanggal, String jam, String shift, String ket, String foto) {
        this.patrolmanId = patrolman;
        this.eventId = event;
        this.checkpointId = checkpoint;
        this.tanggal = tanggal;
        this.jam = jam;
        this.shift = shift;
        this.keterangan = ket;
        this.foto = foto;
    }

    public int getPatroliId() {
        return patroliId;
    }

    public void setPatroliId(int patroliId) {
        this.patroliId = patroliId;
    }

    public int getPatrolmanId() {
        return patrolmanId;
    }

    public void setPatrolmanId(int patrolmanId) {
        this.patrolmanId = patrolmanId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(int checkpointId) {
        this.checkpointId = checkpointId;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
