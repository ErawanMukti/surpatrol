package project.id.surpatrol.model;

import com.google.gson.annotations.Expose;

public class UploadResponse {
    @Expose
    String status;
    @Expose
    String message;

    public UploadResponse() {}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
