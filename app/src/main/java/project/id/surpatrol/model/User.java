package project.id.surpatrol.model;

public class User {
    public static final String TABLE = "users";

    public static final String COLUMN_ID = "userid";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";

    private int userId;
    private String userName;
    private String userPass;

    public User() {}

    public User(int userid, String username, String password) {
        this.userId = userid;
        this.userName = username;
        this.userPass = password;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }
}
