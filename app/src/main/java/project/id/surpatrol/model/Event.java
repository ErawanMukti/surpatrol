package project.id.surpatrol.model;

public class Event {
    public static final String TABLE = "events";

    public static final String COLUMN_ID = "eventid";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";

    private int eventId;
    private int eventCode;
    private String eventName;

    public Event() { }

    public Event(int id, int code, String name) {
        this.eventId = id;
        this.eventCode = code;
        this.eventName = name;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getEventCode() {
        return eventCode;
    }

    public void setEventCode(int eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @Override
    public String toString() {
        return eventName;
    }
}
