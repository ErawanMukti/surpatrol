package project.id.surpatrol;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DataAreaActivity extends AppCompatActivity {

    List<String> dataArea;
    ListView listData;
    FloatingActionButton fabArea;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_area);

        initializedData();
        initializedResources();
    }

    private void initializedResources() {
        listData = (ListView) findViewById(R.id.listDataArea);
        fabArea  = (FloatingActionButton) findViewById(R.id.fabAddArea);

        adapter  = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, dataArea);
        listData.setAdapter(adapter);

        fabArea.setOnClickListener(new AddAreaListener());
    }

    private void initializedData() {
        dataArea = new ArrayList<>();
        dataArea.add("All Area");
    }

    private class AddAreaListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Toast.makeText(getBaseContext(), "Dalam Proses", Toast.LENGTH_SHORT).show();
        }
    }
}
