package project.id.surpatrol.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import project.id.surpatrol.model.Event;

public class EventTable {
    private Event event;

    public EventTable() {
        event = new Event();
    }

    public static String createTable() {
        return "CREATE TABLE " + Event.TABLE + " (" +
                Event.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Event.COLUMN_CODE + " TEXT, "+
                Event.COLUMN_NAME + " TEXT)";
    }

    public static String initializeData() {
        return "INSERT INTO "+ Event.TABLE +" ( "+ Event.COLUMN_CODE +", "+ Event.COLUMN_NAME +") " +
                "VALUES (21, 'STAIR CASE BLOCKING'), " +
                "(01, 'ALARM SOUND'), " +
                "(02, 'BROKEN DOOR'), " +
                "(34, 'CAR TIER DEFLATED / DAMAGE'), " +
                "(32, 'CAR UNLOCK / WINDOW OPEN'), " +
                "(04, 'CEILING LAMP FUSED'), " +
                "(05, 'CHIP / DIRTY TILE'), " +
                "(06, 'DIRTY STANDING ASTHRY'), " +
                "(36, 'DND'), " +
                "(07, 'DOOR KNOP MENU'), " +
                "(33, 'ENGINEERING MAINTENANCE'), " +
                "(08, 'EXIT DOOR BLOCKING'), " +
                "(09, 'FIRE'), " +
                "(10, 'FOUND SUSPECIOUS ITEM'), " +
                "(11, 'HYDRANT LEAKING'), " +
                "(12, 'LAUNDRY BAG'), " +
                "(13, 'LIGHT OFF'), " +
                "(03, 'LOST ACCESORIES'), " +
                "(14, 'LOST ITEM'), " +
                "(15, 'NOISTY / DRUNK'), " +
                "(29, 'NOT IN PLACE PARKING'), " +
                "(16, 'OFFICE LIGHT OFF'), " +
                "(30, 'PANTRY DIRTY'), " +
                "(17, 'PHONE MULFUNCTION'), " +
                "(18, 'PIPE LEAKING'), " +
                "(19, 'SHOES'), " +
                "(20, 'SPOTY / TEAR CARPET'), " +
                "(22, 'TRAFFIC ACCIDENT'), " +
                "(23, 'TROLY / TRAY'), " +
                "(24, 'UNLOCKED DOOR'), " +
                "(25, 'VANDALISME'), " +
                "(26, 'WALL LAMP FUSED'), " +
                "(27, 'WATCH ALARM'), " +
                "(28, 'WINDOW OPEN'), " +
                "(7, 'DOOR KNOP MENU'), " +
                "(188, 'PIPE LEAKING'), " +
                "(131, 'PROJECT / DEKORASI'), " +
                "(31, 'PROJECT / DECORATION ACTIVITY'), " +
                "(331, 'PROJECT ACTIVITY'), " +
                "(223, 'TROLLY'); ";
    }

    public int insert(Event data) {
        int hasilId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();
        values.put(Event.COLUMN_CODE, data.getEventCode());
        values.put(Event.COLUMN_NAME, data.getEventName());

        // Inserting Row
        hasilId = (int)db.insert(Event.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return hasilId;
    }

    public void clear() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(Event.TABLE,null,null);
        DatabaseManager.getInstance().closeDatabase();
    }

    public List<Event> selectAll(){
        List<Event> hasilList = new ArrayList<>();

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM " + Event.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Event data = new Event();
                data.setEventId(cursor.getInt(cursor.getColumnIndex(Event.COLUMN_ID)));
                data.setEventCode(cursor.getInt(cursor.getColumnIndex(Event.COLUMN_CODE)));
                data.setEventName(cursor.getString(cursor.getColumnIndex(Event.COLUMN_NAME)));

                hasilList.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return hasilList;
    }
}
