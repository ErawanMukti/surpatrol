package project.id.surpatrol.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import project.id.surpatrol.model.Checkpoint;
import project.id.surpatrol.model.Event;
import project.id.surpatrol.model.Patroli;
import project.id.surpatrol.model.User;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "surpatrol";
    private static final String TAG = DBHelper.class.getSimpleName().toString();

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UserTable.createTable());
        db.execSQL(UserTable.initializeData());
        db.execSQL(EventTable.createTable());
        db.execSQL(EventTable.initializeData());
        db.execSQL(CheckpointTable.createTable());
        db.execSQL(CheckpointTable.initializeData());
        db.execSQL(PatroliTable.createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, String.format("SQLiteDatabase.onUpgrade(%d -> %d)", oldVersion, newVersion));

        // Drop table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + User.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Event.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Checkpoint.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Patroli.TABLE);
        onCreate(db);
    }
}
