package project.id.surpatrol.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import project.id.surpatrol.model.User;

public class UserTable {
    private User data;

    public UserTable(){
        data = new User();
    }

    public static String createTable(){
        return "CREATE TABLE " + User.TABLE + " (" +
                User.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                User.COLUMN_USERNAME + " TEXT, "+
                User.COLUMN_PASSWORD + " TEXT)";
    }

    public static String initializeData() {
        return "INSERT INTO "+ User.TABLE +" ("+ User.COLUMN_ID +", "+ User.COLUMN_USERNAME +", "+ User.COLUMN_PASSWORD +") " +
                "VALUES (1, 'hartono', 'hartono'), " +
                "(2, 'jaenuri', 'jaenuri'), " +
                "(3, 'kastari', 'kastari'), " +
                "(4, 'sutaji', 'sutaji'), " +
                "(5, 'wiji', 'wiji'), " +
                "(6, 'ridwan', 'ridwan'); ";
    }

    public int insert(User data) {
        int hasilId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();
        values.put(User.COLUMN_USERNAME, data.getUserName());
        values.put(User.COLUMN_PASSWORD, data.getUserPass());

        // Inserting Row
        hasilId = (int)db.insert(User.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return hasilId;
    }

    public void clear() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(User.TABLE,null,null);
        DatabaseManager.getInstance().closeDatabase();
    }

    public List<User> selectAll(){
        List<User> hasilList = new ArrayList<>();

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM " + User.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                User data = new User();
                data.setUserId(cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID)));
                data.setUserName(cursor.getString(cursor.getColumnIndex(User.COLUMN_USERNAME)));
                data.setUserPass(cursor.getString(cursor.getColumnIndex(User.COLUMN_PASSWORD)));

                hasilList.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return hasilList;
    }

    public User selectSingle(String username, String password) {
        User user = new User();

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * " +
                             "  FROM " + User.TABLE +
                             " WHERE " + User.COLUMN_USERNAME + " = '"+ username +"'" +
                             "   AND " + User.COLUMN_PASSWORD + " = '"+ password +"'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                user.setUserId(cursor.getInt(cursor.getColumnIndex(User.COLUMN_ID)));
                user.setUserName(cursor.getString(cursor.getColumnIndex(User.COLUMN_USERNAME)));
                user.setUserPass(cursor.getString(cursor.getColumnIndex(User.COLUMN_PASSWORD)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return user;
    }
}
