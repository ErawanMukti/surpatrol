package project.id.surpatrol.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import project.id.surpatrol.model.Patroli;

public class PatroliTable {
    private Patroli patroli;

    public PatroliTable() {
        patroli = new Patroli();
    }

    public static String createTable() {
        return "CREATE TABLE " + Patroli.TABLE + " (" +
                Patroli.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Patroli.COLUMN_PATROLMAN + " INTEGER, "+
                Patroli.COLUMN_EVENT + " INTEGER, "+
                Patroli.COLUMN_CHECKPOINT + " INTEGER, "+
                Patroli.COLUMN_TANGGAL + " DATE, "+
                Patroli.COLUMN_JAM + " TEXT, "+
                Patroli.COLUMN_SHIFT + " TEXT, "+
                Patroli.COLUMN_KET + " TEXT," +
                Patroli.COLUMN_FOTO + " TEXT)";
    }

    public int insert(Patroli data) {
        int hasilId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();
        values.put(Patroli.COLUMN_PATROLMAN, data.getPatrolmanId());
        values.put(Patroli.COLUMN_EVENT, data.getEventId());
        values.put(Patroli.COLUMN_CHECKPOINT, data.getCheckpointId());
        values.put(Patroli.COLUMN_TANGGAL, data.getTanggal());
        values.put(Patroli.COLUMN_JAM, data.getJam());
        values.put(Patroli.COLUMN_SHIFT, data.getShift());
        values.put(Patroli.COLUMN_KET, data.getKeterangan());
        values.put(Patroli.COLUMN_FOTO, data.getFoto());

        // Inserting Row
        hasilId = (int)db.insert(Patroli.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return hasilId;
    }

    public void clear() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(Patroli.TABLE,null,null);
        DatabaseManager.getInstance().closeDatabase();
    }

    public List<Patroli> selectAll(){
        List<Patroli> hasilList = new ArrayList<>();

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM " + Patroli.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Patroli data = new Patroli();
                data.setPatroliId(cursor.getInt(cursor.getColumnIndex(Patroli.COLUMN_ID)));
                data.setPatrolmanId(cursor.getInt(cursor.getColumnIndex(Patroli.COLUMN_PATROLMAN)));
                data.setEventId(cursor.getInt(cursor.getColumnIndex(Patroli.COLUMN_EVENT)));
                data.setCheckpointId(cursor.getInt(cursor.getColumnIndex(Patroli.COLUMN_CHECKPOINT)));
                data.setTanggal(cursor.getString(cursor.getColumnIndex(Patroli.COLUMN_TANGGAL)));
                data.setJam(cursor.getString(cursor.getColumnIndex(Patroli.COLUMN_JAM)));
                data.setShift(cursor.getString(cursor.getColumnIndex(Patroli.COLUMN_SHIFT)));
                data.setKeterangan(cursor.getString(cursor.getColumnIndex(Patroli.COLUMN_KET)));
                data.setFoto(cursor.getString(cursor.getColumnIndex(Patroli.COLUMN_FOTO)));

                hasilList.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return hasilList;
    }

    public boolean deleteSingle(int id) {
        boolean success = false;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        success = db.delete(Patroli.TABLE, Patroli.COLUMN_ID + "=" + id, null) > 0;
        DatabaseManager.getInstance().closeDatabase();

        return success;
    }
}
