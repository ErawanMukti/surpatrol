package project.id.surpatrol.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import project.id.surpatrol.model.Checkpoint;

public class CheckpointTable {
    private Checkpoint checkpoint;

    public CheckpointTable() {
        checkpoint = new Checkpoint();
    }

    public static String createTable() {
        return "CREATE TABLE " + Checkpoint.TABLE + " (" +
                Checkpoint.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Checkpoint.COLUMN_GROUP + " TEXT, "+
                Checkpoint.COLUMN_CODE + " TEXT, "+
                Checkpoint.COLUMN_NAME + " TEXT)";
    }

    public static String initializeData() {
        return "INSERT INTO "+ Checkpoint.TABLE +" ( "+ Checkpoint.COLUMN_GROUP +", "+ Checkpoint.COLUMN_CODE +", "+ Checkpoint.COLUMN_NAME +") " +
                "VALUES ('INDOOR', '0A14000AD9', 'B1 - SKILL HALL'), " +
                "('INDOOR', '0A140004FA', 'B1 - ACCOUNTING'), " +
                "('INDOOR', '0417519574', 'B1 - AMENITY STORE / FC FILE ROOM'), " +
                "('INDOOR', '0A14000503', 'B1 - ENGINEERING'), " +
                "('INDOOR', '041624CA3B', 'B1 - IT OFFICE'), " +
                "('INDOOR', '041624CB40', 'B1 - SERVICE LIFT'), " +
                "('INDOOR', '041751D245', 'B1 - STAFF CANTEEN'), " +
                "('INDOOR', '041624C8B3', 'B1 - STAFF MALE LOCKER'), " +
                "('INDOOR', '04191F2B25', 'B1 - GENERATOR ROOM / BOILER / TRANSFORMER ROOM'), " +
                "('INDOOR', '041624C7EA', 'B1 - HRD / LVMDP'), " +
                "('INDOOR', '040624CA3B', 'IT OFFICE'), " +
                "('INDOOR', '0A140004DA', 'LV - 10'), " +
                "('INDOOR', '0A14000AC0', 'LV - 11'), " +
                "('INDOOR', '0A14000500', 'LV - 14'), " +
                "('INDOOR', '0A14000ABC', 'LV - 17'), " +
                "('INDOOR', '0A140004DF', 'LV - 4'), " +
                "('INDOOR', '0A140004F8', 'LV - 5'), " +
                "('INDOOR', '0A140004C3', 'LV - 6'), " +
                "('INDOOR', '0A140004D1', 'LV - 7'), " +
                "('INDOOR', '0A140004F7', 'LV - 8'), " +
                "('INDOOR', '0A140004CD', 'LV - 9'), " +
                "('INDOOR', '0A140004D9', 'LV - 12'), " +
                "('INDOOR', '0A14000AC2', 'LV - 15'), " +
                "('INDOOR', '0A14000AC6', 'LV - 16'), " +
                "('INDOOR', '0A14000ADA', 'LV 1 - BALLROOM'), " +
                "('INDOOR', '04191F3F2A', 'LV 1 - BANQUET KITCHEN'), " +
                "('INDOOR', '041624C874', 'LV 1 - BANQUET STORE'), " +
                "('INDOOR', '041624DB0E', 'LV 1 - MAIN KITCHEN'), " +
                "('INDOOR', '0A140004E3', 'LV 1 - PURCHASING'), " +
                "('INDOOR', '04191F42D8', 'LV 1 - SUPER STORE'), " +
                "('INDOOR', '0A14000B1C', 'LV 1 - SWIMMING POOL'), " +
                "('INDOOR', '041624DC15', 'LV 2 - NISHIMURA'), " +
                "('INDOOR', '041624D8EC', 'LV 2 - PELANGI'), " +
                "('INDOOR', '041624C81D', 'LV 2 - PORTOFINO'), " +
                "('INDOOR', '0A14000AC4', 'LV 2 - NIRWANA'), " +
                "('INDOOR', '0A140004FC', 'LV 2 - RECEPTION'), " +
                "('INDOOR', '041624DB91', 'LV 3 - GM OFFICE'), " +
                "('INDOOR', '0A140004F3', 'LV 3 - HEALTH CLUB'), " +
                "('INDOOR', '04191F5447', 'LV 3 - SAUNA & JACCUZI ROOM - FEMALE'), " +
                "('INDOOR', '04191F1E05', 'LV 3 - SAUNA & JACCUZI ROOM - MALE'), " +
                "('INDOOR', '04191F44E9', 'LV 3 - SUMATRA ROOM'), " +
                "('INDOOR', '041624C8FC', 'LV 3 - TENNIS COURT'), " +
                "('INDOOR', '0A14000ADB', 'LV 1 - DESPERADO'), " +
                "('INDOOR', '041624CADB', 'LV 1 - JAMOO KITCHEN'), " +
                "('INDOOR', '041624CD4F', 'ROOF - A'), " +
                "('INDOOR', '041624C9A4', 'ROOF - B'), " +
                "('OUTDOOR', '041751A664', 'CARPARK (BUILDING) - LV 2'), " +
                "('OUTDOOR', '0463000D3A', 'CARPARK (BUILDING) - LV 1'), " +
                "('OUTDOOR', '0463000D2D', 'CARPARK (BUILDING) - LV 3'), " +
                "('OUTDOOR', '041751DC8E', 'CARPARK (BUILDING) - LV 4'), " +
                "('OUTDOOR', '0463000D25', 'CARPARK (BUILDING) - LV 5'), " +
                "('OUTDOOR', '0463000D28', 'CARPARK (BUILDING) - LV 6'), " +
                "('OUTDOOR', '041624C900', 'CHEMICAL STORE'), " +
                "('OUTDOOR', '04191F21FF', 'DRIVER MUSHOLA'), " +
                "('OUTDOOR', '041624CA90', 'FUEL STATION'), " +
                "('OUTDOOR', '041624CC3F', 'LV 1 - BALLROOM CAR CALL'), " +
                "('OUTDOOR', '04191F3603', 'MAIN PARKING AREA - LOT A'), " +
                "('OUTDOOR', '041751B5E0', 'MAIN PARKING AREA - LOT B'), " +
                "('OUTDOOR', '04191F2368', 'MAIN PARKING AREA - LOT C'), " +
                "('OUTDOOR', '041751BD9A', 'MAIN PARKING AREA - LOT D'), " +
                "('OUTDOOR', '04191F2C0F', 'MAIN PARKING AREA - LOT E'), " +
                "('OUTDOOR', '04175199A5', 'MAIN PARKING AREA - LOT F'), " +
                "('OUTDOOR', '041624C8D9', 'MOTORCYCLE KOPERASI'), " +
                "('OUTDOOR', '0A140004D3', 'POST - 1'), " +
                "('OUTDOOR', '0A14000AC1', 'POST - 2'), " +
                "('OUTDOOR', '0A14000AD8', 'POST - 3'), " +
                "('OUTDOOR', '0A140004F0', 'POST 4 / LPG TANK'); ";
    }

    public int insert(Checkpoint data) {
        int hasilId;

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();
        values.put(Checkpoint.COLUMN_GROUP, data.getCpGroup());
        values.put(Checkpoint.COLUMN_CODE, data.getCpCode());
        values.put(Checkpoint.COLUMN_NAME, data.getCpName());

        // Inserting Row
        hasilId = (int)db.insert(Checkpoint.TABLE, null, values);
        DatabaseManager.getInstance().closeDatabase();

        return hasilId;
    }

    public void clear() {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(Checkpoint.TABLE,null,null);
        DatabaseManager.getInstance().closeDatabase();
    }

    public List<Checkpoint> selectAll(){
        List<Checkpoint> hasilList = new ArrayList<>();

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM " + Checkpoint.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Checkpoint data = new Checkpoint();
                data.setCpId(cursor.getInt(cursor.getColumnIndex(Checkpoint.COLUMN_ID)));
                data.setCpGroup(cursor.getString(cursor.getColumnIndex(Checkpoint.COLUMN_GROUP)));
                data.setCpCode(cursor.getInt(cursor.getColumnIndex(Checkpoint.COLUMN_CODE)));
                data.setCpName(cursor.getString(cursor.getColumnIndex(Checkpoint.COLUMN_NAME)));

                hasilList.add(data);
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return hasilList;
    }

    public Checkpoint selectSingleByCode(String code) {
        Checkpoint checkpoint = new Checkpoint();

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT * FROM " + Checkpoint.TABLE + " WHERE " + Checkpoint.COLUMN_CODE + " = '" + code + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                checkpoint.setCpId(cursor.getInt(cursor.getColumnIndex(Checkpoint.COLUMN_ID)));
                checkpoint.setCpGroup(cursor.getString(cursor.getColumnIndex(Checkpoint.COLUMN_GROUP)));
                checkpoint.setCpCode(cursor.getInt(cursor.getColumnIndex(Checkpoint.COLUMN_CODE)));
                checkpoint.setCpName(cursor.getString(cursor.getColumnIndex(Checkpoint.COLUMN_NAME)));
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return checkpoint;
    }

    public String getCheckpointName(int id) {
        String nama = "";

        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        String selectQuery = "SELECT "+ Checkpoint.COLUMN_NAME +" FROM " + Checkpoint.TABLE + " WHERE " + Checkpoint.COLUMN_ID + " = '" + id + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                nama = cursor.getString(cursor.getColumnIndex(Checkpoint.COLUMN_NAME));
            } while (cursor.moveToNext());
        }

        cursor.close();
        DatabaseManager.getInstance().closeDatabase();

        return nama;
    }
}
