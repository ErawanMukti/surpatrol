package project.id.surpatrol;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import project.id.surpatrol.databases.CheckpointTable;
import project.id.surpatrol.model.Checkpoint;

public class DataCheckpointActivity extends AppCompatActivity {

    List<Checkpoint> dataCheckpoint;
    ListView listData;
    FloatingActionButton fabCheckpoint;
    ArrayAdapter<Checkpoint> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_checkpoint);

        initializedData();
        initializedResources();
    }

    private void initializedResources() {
        listData      = (ListView) findViewById(R.id.listDataCheckpoint);
        fabCheckpoint = (FloatingActionButton) findViewById(R.id.fabAddCheckpoint);

        adapter  = new ArrayAdapter<Checkpoint>(getBaseContext(), android.R.layout.simple_list_item_1, dataCheckpoint);
        listData.setAdapter(adapter);

        fabCheckpoint.setOnClickListener(new AddCheckpointListener());
    }

    private void initializedData() {
        CheckpointTable checkpointTable = new CheckpointTable();
        dataCheckpoint = checkpointTable.selectAll();
    }

    private class AddCheckpointListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Toast.makeText(getBaseContext(), "Dalam Proses", Toast.LENGTH_SHORT).show();
        }
    }}
