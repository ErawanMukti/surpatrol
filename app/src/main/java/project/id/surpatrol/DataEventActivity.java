package project.id.surpatrol;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DataEventActivity extends AppCompatActivity {

    List<String> dataEvent;
    ListView listData;
    FloatingActionButton fabEvent;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_event);

        initializedData();
        initializedResources();
    }

    private void initializedResources() {
        listData = (ListView) findViewById(R.id.listDataEvent);
        fabEvent = (FloatingActionButton) findViewById(R.id.fabAddEvent);

        adapter  = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, dataEvent);
        listData.setAdapter(adapter);

        fabEvent.setOnClickListener(new AddEventListener());
    }

    private void initializedData() {
        dataEvent = new ArrayList<>();
        dataEvent.add("STAIR CASE BLOCKING");
        dataEvent.add("ALARM SOUND");
        dataEvent.add("BROKEN DOOR");
        dataEvent.add("CAR TIER DEFLATED / DAMAGE");
        dataEvent.add("CAR UNLOCK / WINDOW OPEN");
        dataEvent.add("CEILLING LAMP FUSED");
        dataEvent.add("CHIP / DIRTY TILE");
        dataEvent.add("DIRTY STANDING ASTHRY");
        dataEvent.add("DND");
        dataEvent.add("DOOR KNOP MENU");
        dataEvent.add("ENGINEERING MAINTENANCE");
        dataEvent.add("EXIT DOOR BLOCKING");
        dataEvent.add("FIRE");
        dataEvent.add("FOUND SUSPECIOUS ITEM");
        dataEvent.add("HYDRANT LEAKING");
    }

    private class AddEventListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Toast.makeText(getBaseContext(), "Dalam Proses", Toast.LENGTH_SHORT).show();
        }
    }}
