package project.id.surpatrol.rest;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import project.id.surpatrol.model.UploadResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("upload")
    Call<UploadResponse> postUpload(
            @Field("patrolmanid") int userid,
            @Field("eventid") int eventid,
            @Field("checkpointid") int checkpointid,
            @Field("tanggal") String tanggal,
            @Field("jam") String jam,
            @Field("shift") String shift,
            @Field("ket") String ket
    );

    @Multipart
    @POST("upload")
    Call<ResponseBody> uploadPatroli(
            @Part("hasilpatroli") RequestBody hasilpatroli,
            @Part List<MultipartBody.Part> files
    );
}
