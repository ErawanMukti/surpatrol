package project.id.surpatrol;

import android.app.Application;
import android.content.Context;

import project.id.surpatrol.databases.DBHelper;
import project.id.surpatrol.databases.DatabaseManager;

public class MyApp extends Application {
    private static Context context;
    private static DBHelper dbHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();
        dbHelper = new DBHelper(context);
        DatabaseManager.initializeInstance(dbHelper);
    }

    public static Context getContext() {
        return context;
    }
}
