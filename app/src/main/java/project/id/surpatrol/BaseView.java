package project.id.surpatrol;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
